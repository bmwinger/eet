#[macro_use]
extern crate clap;
#[macro_use]
extern crate anyhow;

use anyhow::{Context, Result};
use clap::{App, SubCommand};
use derive_more::{Display, From};
use encoding::all::{UTF_8, WINDOWS_1252};
use encoding::{DecoderTrap, Encoding};
use hashlink::LinkedHashMap;
use nom::bytes::complete::take_while;
use serde::Serialize;
use std::borrow::Cow;
use std::fs;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::{Path, PathBuf};
use thiserror::Error;

#[derive(Error, From, Debug, Display)]
pub enum EETError {
    YamlError(serde_yaml::Error),
    #[display(fmt = "Parsing Error: {:?} {:?}", _0, _1)]
    #[from(ignore)]
    ParsingError(Vec<u8>, nom::error::ErrorKind),
    #[display(fmt = "Parsing Failure: {:?} {:?}", _0, _1)]
    #[from(ignore)]
    ParsingFailure(Vec<u8>, nom::error::ErrorKind),
    #[display(fmt = "Parsing Incomplete: {:?}", _0)]
    #[from(ignore)]
    ParsingIncomplete(nom::Needed),
    FromUtf8Error(std::string::FromUtf8Error),
    EncodingError(Cow<'static, str>),
}

impl From<nom::Err<nom::error::Error<&[u8]>>> for EETError {
    fn from(other: nom::Err<nom::error::Error<&[u8]>>) -> Self {
        match other {
            nom::Err::Error(e) => EETError::ParsingError(e.input.to_vec(), e.code),
            nom::Err::Incomplete(i) => EETError::ParsingIncomplete(i),
            nom::Err::Failure(e) => EETError::ParsingFailure(e.input.to_vec(), e.code),
        }
    }
}

pub fn take_nt_str_exact<'a>(
    data: &'a [u8],
    size: usize,
    encoding: &'static dyn Encoding,
) -> Result<(&'a [u8], String), EETError> {
    let (_, string) = if data.len() > size {
        take_while(|x| x != 0)(&data[0..size])?
    } else {
        take_while(|x| x != 0)(data)?
    };
    //let result = String::from_utf8(string.to_vec())?;
    let result = encoding.decode(string, DecoderTrap::Strict)?;
    Ok((&data[size..], result))
}

pub fn take_len_string(data: &[u8]) -> Result<(&[u8], String), anyhow::Error> {
    take_len_string_encoding(data, UTF_8)
}

pub fn take_win_len_string(data: &[u8]) -> Result<(&[u8], String), anyhow::Error> {
    take_len_string_encoding(data, WINDOWS_1252)
}

pub fn take_len_string_encoding<'a>(
    data: &'a [u8],
    encoding: &'static dyn Encoding,
) -> Result<(&'a [u8], String), anyhow::Error> {
    let (data, size) = le_i32(data)?;
    if size > 0 {
        if size as usize > data.len() {
            return Err(anyhow!("String had unexpected length: {}", size,));
        }
        let (data, string) = take_nt_str_exact(data, size as usize, encoding)?;
        Ok((data, string))
    } else {
        Ok((data, String::new()))
    }
}

pub fn take_i16_len_string(data: &[u8]) -> Result<(&[u8], String), anyhow::Error> {
    let (data, size) = le_i16(data)?;
    if size > 0 {
        if size as usize > data.len() {
            return Err(anyhow!("String had unexpected length: {}", size));
        }
        let (data, string) = take_nt_str_exact(data, size as usize, UTF_8)?;
        Ok((data, string))
    } else {
        Ok((data, String::new()))
    }
}

fn le_i32(data: &[u8]) -> Result<(&[u8], i32), anyhow::Error> {
    use nom::number::complete::le_i32;
    Ok(le_i32::<_, nom::error::Error<_>>(data).map_err(|x| EETError::from(x))?)
}

fn le_i16(data: &[u8]) -> Result<(&[u8], i16), anyhow::Error> {
    use nom::number::complete::le_i16;
    Ok(le_i16::<_, nom::error::Error<_>>(data).map_err(|x| EETError::from(x))?)
}

#[derive(Serialize, Hash, PartialEq, Eq)]
struct MappingTarget {
    record_type: String,
    sub_type: String,
}

#[derive(Serialize, Hash, PartialEq, Eq)]
#[serde(tag = "type")]
enum Mapping {
    StringMapping {
        targets: Vec<MappingTarget>,
        mapping: LinkedHashMap<String, String>,
    },
}

#[derive(Serialize)]
struct MappingFile {
    source_lang: String,
    dest_lang: String,
    mappings: Vec<Mapping>,
}

#[derive(Serialize)]
struct Entry {
    typ: String,
    identifier1: String,
    identifier2: String,
    subrecord: String,
    source: String,
    dest: String,
    unknown1: String,
    // Bool indicating an identifier rename?
    unknown2: i32,
    unknown3: i16,
    unknown4: i32,
    unknown5: String,
    unknown6: i32,
    squished_source: String,
    unknown7: i32,
    // Note: Does not appear to be utf-8. Can be parsed by win1252
    unknown8: String,
    // Note: Does not appear to be utf-8. Can be parsed by win1252
    unknown9: String,
    unknown10: String,
}

#[derive(Serialize)]
struct EET {
    unknown1: i32,
    unknown2: i32,
    unknown3: String,
    unknown4: String,
    entries: Vec<Entry>,
    // Possibly more unknown numeric variables
    unknown5: Vec<u8>,
    // Mappings of the squished identifier to the translation
    // Encoding is unusual
    mapping: LinkedHashMap<String, String>,
}

fn take_entry(data: &[u8]) -> Result<(&[u8], Entry), anyhow::Error> {
    let (data, _len) = le_i32(data)?;
    let (data, typ) = take_len_string(data).context("When reading entry type")?;
    let (data, identifier1) = take_len_string(data).context("When reading identifier1")?;
    let (data, identifier2) = take_len_string(data).context("When reading identifier2")?;
    let (data, subrecord) = take_len_string(data).context("When reading subrecord type")?;
    let (data, source) = take_len_string(data).context("When reading source")?;
    let (data, dest) = take_len_string(data).context("When reading dest")?;
    let (data, unknown1) = take_len_string(data).context("when reading unknown1")?;
    let (data, unknown2) = le_i32(data).context("when reading unknown2")?;
    let (data, unknown3) = le_i16(data).context("when reading unknown3")?;
    let (data, unknown4) = le_i32(data).context("when reading unknown4")?;
    let (data, unknown5) = take_len_string(data).context("when reading unknown5")?;
    let (data, unknown6) = le_i32(data).context("when reading unknown6")?;
    let (data, squished_source) = take_len_string(data).context("when reading squished_source")?;
    let (data, unknown7) = le_i32(data).context("when reading unknown7")?;
    let (data, unknown8) = take_win_len_string(data).context("when reading unknown8")?;
    let (data, unknown9) = take_win_len_string(data).context("when reading unknown9")?;
    let (data, unknown10) = take_len_string(data).context("when reading unknown10")?;

    Ok((
        data,
        Entry {
            typ,
            identifier1,
            identifier2,
            subrecord,
            source,
            dest,
            unknown1,
            unknown2,
            unknown3,
            unknown4,
            unknown5,
            unknown6,
            squished_source,
            unknown7,
            unknown8,
            unknown9,
            unknown10,
        },
    ))
}

fn parse_eet(data: &[u8]) -> Result<EET, anyhow::Error> {
    let magic = &data[0..4];
    let data = &data[4..];
    if magic != b"EET_" {
        return Err(anyhow!("Incorrect Magic Number!"));
    }
    let (data, unknown1) = le_i32(data)?;
    let (data, unknown2) = le_i32(data)?;
    let unknown3 = String::from_utf8(data[0..4].to_vec())?;
    // Skip two bytes of seemingly blank padding
    let unknown4 = String::from_utf8(data[6..10].to_vec())?;
    let data = &data[10..];
    let (mut data, len) = le_i32(data)?;
    let mut entries = vec![];
    for i in 0..len {
        let (_data, entry) = take_entry(data).context(format!("In entry {}", i))?;
        data = _data;
        entries.push(entry);
    }

    // Unknown magic string quartet
    let phra = String::from_utf8(data[0..4].to_vec())?;
    if phra != "PHRA" {
        eprintln!("Magic PHRA key preceeding mapping was actually {}", phra);
    }
    let unknown5 = data[4..12].to_vec();
    let mut mapping = LinkedHashMap::new();
    let mut data = &data[12..];
    while data.len() > 0 {
        let (_data, source) = take_i16_len_string(data).context("when reading mapping key")?;
        let (_data, dest) = take_i16_len_string(_data).context("when reading mapping value")?;
        data = _data;
        mapping.insert(source, dest);
    }

    if data.len() > 0 {
        eprintln!("Remaining data was nonempty with length {}!", data.len());
    }

    Ok(EET {
        unknown1,
        unknown2,
        unknown3,
        unknown4,
        entries,
        unknown5,
        mapping,
    })
}

fn parse(filename: &str) -> Result<()> {
    let f = File::open(filename)?;
    let mut reader = BufReader::new(f);
    let mut buffer = Vec::new();

    // Read file into vector.
    reader.read_to_end(&mut buffer)?;
    let eet = parse_eet(&buffer)?;

    let out_str = serde_json::to_string(&eet)?;
    let file = Path::new(filename);

    println!(
        "Writing result to {}...",
        file.with_extension("json").display()
    );
    fs::write(file.with_extension("json"), out_str)?;
    Ok(())
}

fn create_mapping(source_lang: &str, dest_lang: &str, filename: &str) -> Result<()> {
    let f = File::open(filename)?;
    let mut reader = BufReader::new(f);
    let mut buffer = Vec::new();

    // Read file into vector.
    reader.read_to_end(&mut buffer)?;
    let eet = parse_eet(&buffer)?;
    let mut cell_mapping = LinkedHashMap::new();
    let mut dial_mapping = LinkedHashMap::new();

    for entry in eet.entries {
        match (entry.typ.as_str(), entry.subrecord.as_str()) {
            ("DIAL", "NAME") => {
                if entry.source != entry.dest {
                    dial_mapping.insert(entry.source, entry.dest);
                }
            }
            ("CELL", "NAME") => {
                if entry.source != entry.dest {
                    cell_mapping.insert(entry.source, entry.dest);
                }
            }
            _ => (),
        }
    }

    let file = MappingFile {
        source_lang: source_lang.to_string(),
        dest_lang: dest_lang.to_string(),
        mappings: vec![
            Mapping::StringMapping {
                targets: vec![MappingTarget {
                    record_type: "DIAL".to_string(),
                    sub_type: "NAME".to_string(),
                }],
                mapping: dial_mapping,
            },
            Mapping::StringMapping {
                targets: vec![MappingTarget {
                    record_type: "CELL".to_string(),
                    sub_type: "NAME".to_string(),
                }],
                mapping: cell_mapping,
            },
        ],
    };

    let out_str = serde_json::to_string(&file)?;
    let mut file = PathBuf::new();
    file.push(format!("{}-{}-mapping.json", source_lang, dest_lang));

    println!(
        "Writing result to {}...",
        file.with_extension("json").display()
    );
    fs::write(file.with_extension("json"), out_str)?;

    Ok(())
}

fn main() -> Result<()> {
    let matches = App::new(crate_name!())
        .version("0.1.0")
        .author(crate_authors!())
        .about(crate_description!())
        .subcommand(
            SubCommand::with_name("convert")
                .about("Converts the eet file to json")
                .arg_from_usage("[FILE] ... 'Files to be converted'"),
        )
        .subcommand(
            SubCommand::with_name("mapping")
                .about(
                    "Creates an identifier mapping for use with esmap, \
                    mapping Dialogue topics and Interior cell names.",
                )
                .arg_from_usage(
                    "<source_lang> 'Source language of the input file (IETF language tag)'",
                )
                .arg_from_usage("<dest_lang> 'Destination language of the input file'")
                .arg_from_usage("[FILE] ... 'Files to be converted'"),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("convert") {
        if let Some(plugins) = matches.values_of("FILE") {
            for filename in plugins {
                parse(filename)?;
            }
        }
    }
    if let Some(matches) = matches.subcommand_matches("mapping") {
        if let Some(plugins) = matches.values_of("FILE") {
            for filename in plugins {
                create_mapping(
                    matches.value_of("source_lang").unwrap(),
                    matches.value_of("dest_lang").unwrap(),
                    filename,
                )?;
            }
        }
    }
    Ok(())
}
