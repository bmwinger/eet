# EET

A Rust-based .eet file parser for the dictionary files included with the [ESP-ESM Translator](https://www.nexusmods.com/skyrimspecialedition/mods/921).

Designed primarily to extract mappings for use with [ESMap](https://gitlab.com/bmwinger/esmap) to handle inconsistencies in the identifiers between different localisations of Morrowind. 
